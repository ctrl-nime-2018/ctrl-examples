{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 147.0, 79.0, 742.0, 700.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-19",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 230.0, 572.0, 267.0, 33.0 ],
					"style" : "",
					"text" : "<< data saved with patcher, check inspector properties to switch off if required."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"linecount" : 4,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 459.0, 281.0, 150.0, 60.0 ],
					"style" : "",
					"text" : "creates a Coll for CTRL\n\nNeeds some work in terms of initialisation!"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 348.0, 204.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"coll_data" : 					{
						"count" : 111,
						"data" : [ 							{
								"key" : 0,
								"value" : [ 0 ]
							}
, 							{
								"key" : 1,
								"value" : [ 596 ]
							}
, 							{
								"key" : 2,
								"value" : [ 1192 ]
							}
, 							{
								"key" : 3,
								"value" : [ 1787 ]
							}
, 							{
								"key" : 4,
								"value" : [ 2383 ]
							}
, 							{
								"key" : 5,
								"value" : [ 2979 ]
							}
, 							{
								"key" : 6,
								"value" : [ 3575 ]
							}
, 							{
								"key" : 7,
								"value" : [ 4170 ]
							}
, 							{
								"key" : 8,
								"value" : [ 4766 ]
							}
, 							{
								"key" : 9,
								"value" : [ 5362 ]
							}
, 							{
								"key" : 10,
								"value" : [ 5958 ]
							}
, 							{
								"key" : 11,
								"value" : [ 6554 ]
							}
, 							{
								"key" : 12,
								"value" : [ 7149 ]
							}
, 							{
								"key" : 13,
								"value" : [ 7745 ]
							}
, 							{
								"key" : 14,
								"value" : [ 8341 ]
							}
, 							{
								"key" : 15,
								"value" : [ 8937 ]
							}
, 							{
								"key" : 16,
								"value" : [ 9532 ]
							}
, 							{
								"key" : 17,
								"value" : [ 10128 ]
							}
, 							{
								"key" : 18,
								"value" : [ 10724 ]
							}
, 							{
								"key" : 19,
								"value" : [ 11320 ]
							}
, 							{
								"key" : 20,
								"value" : [ 11915 ]
							}
, 							{
								"key" : 21,
								"value" : [ 12511 ]
							}
, 							{
								"key" : 22,
								"value" : [ 13107 ]
							}
, 							{
								"key" : 23,
								"value" : [ 13703 ]
							}
, 							{
								"key" : 24,
								"value" : [ 14299 ]
							}
, 							{
								"key" : 25,
								"value" : [ 14894 ]
							}
, 							{
								"key" : 26,
								"value" : [ 15490 ]
							}
, 							{
								"key" : 27,
								"value" : [ 16086 ]
							}
, 							{
								"key" : 28,
								"value" : [ 16682 ]
							}
, 							{
								"key" : 29,
								"value" : [ 17277 ]
							}
, 							{
								"key" : 30,
								"value" : [ 17873 ]
							}
, 							{
								"key" : 31,
								"value" : [ 18469 ]
							}
, 							{
								"key" : 32,
								"value" : [ 19065 ]
							}
, 							{
								"key" : 33,
								"value" : [ 19661 ]
							}
, 							{
								"key" : 34,
								"value" : [ 20256 ]
							}
, 							{
								"key" : 35,
								"value" : [ 20852 ]
							}
, 							{
								"key" : 36,
								"value" : [ 21448 ]
							}
, 							{
								"key" : 37,
								"value" : [ 22044 ]
							}
, 							{
								"key" : 38,
								"value" : [ 22639 ]
							}
, 							{
								"key" : 39,
								"value" : [ 23235 ]
							}
, 							{
								"key" : 40,
								"value" : [ 23831 ]
							}
, 							{
								"key" : 41,
								"value" : [ 24427 ]
							}
, 							{
								"key" : 42,
								"value" : [ 25022 ]
							}
, 							{
								"key" : 43,
								"value" : [ 25618 ]
							}
, 							{
								"key" : 44,
								"value" : [ 26214 ]
							}
, 							{
								"key" : 45,
								"value" : [ 26810 ]
							}
, 							{
								"key" : 46,
								"value" : [ 27406 ]
							}
, 							{
								"key" : 47,
								"value" : [ 28001 ]
							}
, 							{
								"key" : 48,
								"value" : [ 28597 ]
							}
, 							{
								"key" : 49,
								"value" : [ 29193 ]
							}
, 							{
								"key" : 50,
								"value" : [ 29789 ]
							}
, 							{
								"key" : 51,
								"value" : [ 30384 ]
							}
, 							{
								"key" : 52,
								"value" : [ 30980 ]
							}
, 							{
								"key" : 53,
								"value" : [ 31576 ]
							}
, 							{
								"key" : 54,
								"value" : [ 32172 ]
							}
, 							{
								"key" : 55,
								"value" : [ 32768 ]
							}
, 							{
								"key" : 56,
								"value" : [ 33363 ]
							}
, 							{
								"key" : 57,
								"value" : [ 33959 ]
							}
, 							{
								"key" : 58,
								"value" : [ 34555 ]
							}
, 							{
								"key" : 59,
								"value" : [ 35151 ]
							}
, 							{
								"key" : 60,
								"value" : [ 35746 ]
							}
, 							{
								"key" : 61,
								"value" : [ 36342 ]
							}
, 							{
								"key" : 62,
								"value" : [ 36938 ]
							}
, 							{
								"key" : 63,
								"value" : [ 37534 ]
							}
, 							{
								"key" : 64,
								"value" : [ 38129 ]
							}
, 							{
								"key" : 65,
								"value" : [ 38725 ]
							}
, 							{
								"key" : 66,
								"value" : [ 39321 ]
							}
, 							{
								"key" : 67,
								"value" : [ 39917 ]
							}
, 							{
								"key" : 68,
								"value" : [ 40513 ]
							}
, 							{
								"key" : 69,
								"value" : [ 41108 ]
							}
, 							{
								"key" : 70,
								"value" : [ 41704 ]
							}
, 							{
								"key" : 71,
								"value" : [ 42300 ]
							}
, 							{
								"key" : 72,
								"value" : [ 42896 ]
							}
, 							{
								"key" : 73,
								"value" : [ 43491 ]
							}
, 							{
								"key" : 74,
								"value" : [ 44087 ]
							}
, 							{
								"key" : 75,
								"value" : [ 44683 ]
							}
, 							{
								"key" : 76,
								"value" : [ 45279 ]
							}
, 							{
								"key" : 77,
								"value" : [ 45875 ]
							}
, 							{
								"key" : 78,
								"value" : [ 46470 ]
							}
, 							{
								"key" : 79,
								"value" : [ 47066 ]
							}
, 							{
								"key" : 80,
								"value" : [ 47662 ]
							}
, 							{
								"key" : 81,
								"value" : [ 48258 ]
							}
, 							{
								"key" : 82,
								"value" : [ 48853 ]
							}
, 							{
								"key" : 83,
								"value" : [ 49449 ]
							}
, 							{
								"key" : 84,
								"value" : [ 50045 ]
							}
, 							{
								"key" : 85,
								"value" : [ 50641 ]
							}
, 							{
								"key" : 86,
								"value" : [ 51236 ]
							}
, 							{
								"key" : 87,
								"value" : [ 51832 ]
							}
, 							{
								"key" : 88,
								"value" : [ 52428 ]
							}
, 							{
								"key" : 89,
								"value" : [ 53024 ]
							}
, 							{
								"key" : 90,
								"value" : [ 53620 ]
							}
, 							{
								"key" : 91,
								"value" : [ 54215 ]
							}
, 							{
								"key" : 92,
								"value" : [ 54811 ]
							}
, 							{
								"key" : 93,
								"value" : [ 55407 ]
							}
, 							{
								"key" : 94,
								"value" : [ 56003 ]
							}
, 							{
								"key" : 95,
								"value" : [ 56598 ]
							}
, 							{
								"key" : 96,
								"value" : [ 57194 ]
							}
, 							{
								"key" : 97,
								"value" : [ 57790 ]
							}
, 							{
								"key" : 98,
								"value" : [ 58386 ]
							}
, 							{
								"key" : 99,
								"value" : [ 58982 ]
							}
, 							{
								"key" : 100,
								"value" : [ 59577 ]
							}
, 							{
								"key" : 101,
								"value" : [ 60173 ]
							}
, 							{
								"key" : 102,
								"value" : [ 60769 ]
							}
, 							{
								"key" : 103,
								"value" : [ 61365 ]
							}
, 							{
								"key" : 104,
								"value" : [ 61960 ]
							}
, 							{
								"key" : 105,
								"value" : [ 62556 ]
							}
, 							{
								"key" : 106,
								"value" : [ 63152 ]
							}
, 							{
								"key" : 107,
								"value" : [ 63748 ]
							}
, 							{
								"key" : 108,
								"value" : [ 64343 ]
							}
, 							{
								"key" : 109,
								"value" : [ 64939 ]
							}
, 							{
								"key" : 110,
								"value" : [ 65535 ]
							}
 ]
					}
,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 81.5, 564.0, 103.0, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 1
					}
,
					"style" : "",
					"text" : "coll CTRL-22-tET"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "int" ],
					"patching_rect" : [ 106.0, 204.0, 122.0, 22.0 ],
					"style" : "",
					"text" : "t b i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 326.0, 462.0, 87.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 287.0, 427.0, 33.0, 22.0 ],
					"style" : "",
					"text" : "int 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 287.0, 393.0, 38.0, 22.0 ],
					"style" : "",
					"text" : "+ 0.5"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-4",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 287.0, 362.0, 79.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 81.5, 517.0, 163.0, 22.0 ],
					"style" : "",
					"text" : "110 65535"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 138.0, 462.0, 168.0, 22.0 ],
					"style" : "",
					"text" : "pack 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 138.0, 242.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 204.0, 82.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 204.0, 123.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "metro 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 261.0, 279.0, 74.0, 22.0 ],
					"style" : "",
					"text" : "595.772773"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 215.0, 279.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 215.0, 242.0, 65.0, 22.0 ],
					"style" : "",
					"text" : "sel 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 204.0, 165.0, 240.0, 22.0 ],
					"style" : "",
					"text" : "counter 0 110"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 354.5, 279.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "float 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 287.0, 312.0, 31.0, 22.0 ],
					"style" : "",
					"text" : "+ 0."
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 296.5, 351.0, 412.0, 351.0, 412.0, 252.0, 364.0, 252.0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"midpoints" : [ 357.5, 237.0, 459.0, 237.0, 459.0, 54.0, 213.5, 54.0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 1 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 1 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-3", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"order" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 1 ],
					"order" : 1,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-9", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-9", 2 ]
				}

			}
 ],
		"dependency_cache" : [  ],
		"autosave" : 0
	}

}
