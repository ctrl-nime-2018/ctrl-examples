# CTRL #

This repository houses example code for CTRL

CTRL is an Octal Eurorack CV/Gate interface that is cross platform (Windows, Linux, OSX) 
and can even work with embedded technologies like Raspberry Pi, or ARM based devices for example
'Android TV Boxes' (running Armbian and Pd)

It leverages standard USB Serial communication functionality at its core.